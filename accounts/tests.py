from django.test import TestCase
from .forms import LoginForm


class EntryModelTest(TestCase):

    def test_string_representation(self):
        entry = LoginForm(email="nazhier@surabayahackerlink.org",password="1q2w3e4r5t")
        self.assertEqual(str(entry), entry.email, entry.password)

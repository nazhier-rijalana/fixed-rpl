-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2017 at 05:28 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rpl1`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts_emailactivation`
--

CREATE TABLE `accounts_emailactivation` (
  `id` int(11) NOT NULL,
  `email` varchar(254) NOT NULL,
  `key` varchar(120) DEFAULT NULL,
  `activated` tinyint(1) NOT NULL,
  `forced_expired` tinyint(1) NOT NULL,
  `expires` int(11) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `update` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts_emailactivation`
--

INSERT INTO `accounts_emailactivation` (`id`, `email`, `key`, `activated`, `forced_expired`, `expires`, `timestamp`, `update`, `user_id`) VALUES
(1, 'nazhier@surabayahackerlink.org', '2y45qkb4c8d39g26vfvqdvo8414uihwgswt3gt2pw', 0, 0, 7, '2017-12-10 12:26:57.784269', '2017-12-10 12:26:57.784304', 1),
(3, 'asdasdas@mail.com', 'x8jsbs8e46e3lrrs6eo0uj185kajpfteneikb', 0, 0, 7, '2017-12-10 12:36:25.452091', '2017-12-10 12:36:25.452220', 3);

-- --------------------------------------------------------

--
-- Table structure for table `accounts_guestemail`
--

CREATE TABLE `accounts_guestemail` (
  `id` int(11) NOT NULL,
  `email` varchar(254) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `update` datetime(6) NOT NULL,
  `timestamp` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `accounts_user`
--

CREATE TABLE `accounts_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `staff` tinyint(1) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts_user`
--

INSERT INTO `accounts_user` (`id`, `password`, `last_login`, `email`, `staff`, `admin`, `timestamp`, `full_name`, `is_active`) VALUES
(1, 'pbkdf2_sha256$36000$KRjLK3bZHGwI$cVTRiVC8QYrubWLnPsespuSVOtlRm1Et4t7z8vXTxag=', '2017-12-11 09:56:49.319864', 'nazhier@surabayahackerlink.org', 1, 1, '2017-12-10 12:26:57.743334', NULL, 1),
(3, 'pbkdf2_sha256$36000$Bl7ur9KhtpWT$nyK3fkHOhLSMkiAiwrgJSGpoBq0B6S6bVzI7VYivGN8=', NULL, 'asdasdas@mail.com', 0, 0, '2017-12-10 12:36:25.287749', 'asdasd', 0);

-- --------------------------------------------------------

--
-- Table structure for table `addresses_address`
--

CREATE TABLE `addresses_address` (
  `id` int(11) NOT NULL,
  `address_type` varchar(120) NOT NULL,
  `address_line_1` varchar(120) NOT NULL,
  `address_line_2` varchar(120) DEFAULT NULL,
  `city` varchar(120) NOT NULL,
  `country` varchar(120) NOT NULL,
  `state` varchar(120) NOT NULL,
  `postal_code` varchar(120) NOT NULL,
  `billing_profile_id` int(11) NOT NULL,
  `name` varchar(120) DEFAULT NULL,
  `nickname` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `analytics_objectviewed`
--

CREATE TABLE `analytics_objectviewed` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(220) DEFAULT NULL,
  `object_id` int(10) UNSIGNED NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `analytics_objectviewed`
--

INSERT INTO `analytics_objectviewed` (`id`, `ip_address`, `object_id`, `timestamp`, `content_type_id`, `user_id`) VALUES
(1, '127.0.0.1', 1, '2017-12-10 14:58:08.116864', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `analytics_usersession`
--

CREATE TABLE `analytics_usersession` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(220) DEFAULT NULL,
  `session_key` varchar(100) DEFAULT NULL,
  `timestamp` datetime(6) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `ended` tinyint(1) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add group', 2, 'add_group'),
(5, 'Can change group', 2, 'change_group'),
(6, 'Can delete group', 2, 'delete_group'),
(7, 'Can add permission', 3, 'add_permission'),
(8, 'Can change permission', 3, 'change_permission'),
(9, 'Can delete permission', 3, 'delete_permission'),
(10, 'Can add content type', 4, 'add_contenttype'),
(11, 'Can change content type', 4, 'change_contenttype'),
(12, 'Can delete content type', 4, 'delete_contenttype'),
(13, 'Can add session', 5, 'add_session'),
(14, 'Can change session', 5, 'change_session'),
(15, 'Can delete session', 5, 'delete_session'),
(16, 'Can add email activation', 6, 'add_emailactivation'),
(17, 'Can change email activation', 6, 'change_emailactivation'),
(18, 'Can delete email activation', 6, 'delete_emailactivation'),
(19, 'Can add user', 7, 'add_user'),
(20, 'Can change user', 7, 'change_user'),
(21, 'Can delete user', 7, 'delete_user'),
(22, 'Can add guest email', 8, 'add_guestemail'),
(23, 'Can change guest email', 8, 'change_guestemail'),
(24, 'Can delete guest email', 8, 'delete_guestemail'),
(25, 'Can add address', 9, 'add_address'),
(26, 'Can change address', 9, 'change_address'),
(27, 'Can delete address', 9, 'delete_address'),
(28, 'Can add user session', 10, 'add_usersession'),
(29, 'Can change user session', 10, 'change_usersession'),
(30, 'Can delete user session', 10, 'delete_usersession'),
(31, 'Can add Object viewed', 11, 'add_objectviewed'),
(32, 'Can change Object viewed', 11, 'change_objectviewed'),
(33, 'Can delete Object viewed', 11, 'delete_objectviewed'),
(34, 'Can add charge', 12, 'add_charge'),
(35, 'Can change charge', 12, 'change_charge'),
(36, 'Can delete charge', 12, 'delete_charge'),
(37, 'Can add billing profile', 13, 'add_billingprofile'),
(38, 'Can change billing profile', 13, 'change_billingprofile'),
(39, 'Can delete billing profile', 13, 'delete_billingprofile'),
(40, 'Can add card', 14, 'add_card'),
(41, 'Can change card', 14, 'change_card'),
(42, 'Can delete card', 14, 'delete_card'),
(43, 'Can add cart', 15, 'add_cart'),
(44, 'Can change cart', 15, 'change_cart'),
(45, 'Can delete cart', 15, 'delete_cart'),
(46, 'Can add marketing preference', 16, 'add_marketingpreference'),
(47, 'Can change marketing preference', 16, 'change_marketingpreference'),
(48, 'Can delete marketing preference', 16, 'delete_marketingpreference'),
(49, 'Can add order', 17, 'add_order'),
(50, 'Can change order', 17, 'change_order'),
(51, 'Can delete order', 17, 'delete_order'),
(52, 'Can add product purchase', 18, 'add_productpurchase'),
(53, 'Can change product purchase', 18, 'change_productpurchase'),
(54, 'Can delete product purchase', 18, 'delete_productpurchase'),
(55, 'Can add product', 19, 'add_product'),
(56, 'Can change product', 19, 'change_product'),
(57, 'Can delete product', 19, 'delete_product'),
(58, 'Can add product file', 20, 'add_productfile'),
(59, 'Can change product file', 20, 'change_productfile'),
(60, 'Can delete product file', 20, 'delete_productfile'),
(61, 'Can add tag', 21, 'add_tag'),
(62, 'Can change tag', 21, 'change_tag'),
(63, 'Can delete tag', 21, 'delete_tag');

-- --------------------------------------------------------

--
-- Table structure for table `billing_billingprofile`
--

CREATE TABLE `billing_billingprofile` (
  `id` int(11) NOT NULL,
  `email` varchar(254) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `update` datetime(6) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `customer_id` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing_billingprofile`
--

INSERT INTO `billing_billingprofile` (`id`, `email`, `active`, `update`, `timestamp`, `user_id`, `customer_id`) VALUES
(1, 'asdasdas@mail.com', 1, '2017-12-10 12:36:32.985898', '2017-12-10 12:36:32.986102', 3, 'cus_BvKimvWo8gBVjC'),
(2, 'nazhier@surabayahackerlink.org', 1, '2017-12-11 10:19:19.904308', '2017-12-11 10:19:19.904419', 1, 'cus_BvfioEiwv2otpL');

-- --------------------------------------------------------

--
-- Table structure for table `billing_card`
--

CREATE TABLE `billing_card` (
  `id` int(11) NOT NULL,
  `stripe_id` varchar(120) NOT NULL,
  `brand` varchar(120) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `exp_month` int(11) DEFAULT NULL,
  `exp_year` int(11) DEFAULT NULL,
  `last4` varchar(4) DEFAULT NULL,
  `billing_profile_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `timestamp` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `billing_charge`
--

CREATE TABLE `billing_charge` (
  `id` int(11) NOT NULL,
  `stripe_id` varchar(120) NOT NULL,
  `paid` tinyint(1) NOT NULL,
  `refunded` tinyint(1) NOT NULL,
  `outcome` longtext,
  `outcome_type` varchar(120) DEFAULT NULL,
  `seller_message` varchar(120) DEFAULT NULL,
  `risk_level` varchar(120) DEFAULT NULL,
  `billing_profile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `carts_cart`
--

CREATE TABLE `carts_cart` (
  `id` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `subtotal` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts_cart`
--

INSERT INTO `carts_cart` (`id`, `total`, `updated`, `timestamp`, `user_id`, `subtotal`) VALUES
(1, '0.00', '2017-12-10 12:28:26.946036', '2017-12-10 12:26:31.412522', 1, '0.00'),
(2, '0.00', '2017-12-10 14:58:06.144609', '2017-12-10 12:45:43.098362', 1, '0.00'),
(3, '0.00', '2017-12-11 10:26:07.007066', '2017-12-10 15:06:41.631522', 1, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `carts_cart_products`
--

CREATE TABLE `carts_cart_products` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2017-12-10 14:52:40.419016', '1', 'sadasd', 1, '[{"added": {}}]', 19, 1),
(2, '2017-12-10 14:58:00.386499', '1', 'sadasd', 2, '[]', 19, 1),
(3, '2017-12-11 10:26:48.247029', '5', 'test', 1, '[{"added": {}}]', 19, 1),
(4, '2017-12-11 10:27:47.106538', '6', 'Jamano 2', 1, '[{"added": {}}]', 19, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(6, 'accounts', 'emailactivation'),
(8, 'accounts', 'guestemail'),
(7, 'accounts', 'user'),
(9, 'addresses', 'address'),
(1, 'admin', 'logentry'),
(11, 'analytics', 'objectviewed'),
(10, 'analytics', 'usersession'),
(2, 'auth', 'group'),
(3, 'auth', 'permission'),
(13, 'billing', 'billingprofile'),
(14, 'billing', 'card'),
(12, 'billing', 'charge'),
(15, 'carts', 'cart'),
(4, 'contenttypes', 'contenttype'),
(16, 'marketing', 'marketingpreference'),
(17, 'orders', 'order'),
(18, 'orders', 'productpurchase'),
(19, 'products', 'product'),
(20, 'products', 'productfile'),
(5, 'sessions', 'session'),
(21, 'tags', 'tag');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'accounts', '0001_initial', '2017-12-10 12:20:30.918291'),
(2, 'accounts', '0002_user_full_name', '2017-12-10 12:20:31.599158'),
(3, 'accounts', '0003_user_is_active', '2017-12-10 12:20:32.313986'),
(4, 'accounts', '0004_remove_user_active', '2017-12-10 12:20:33.072660'),
(5, 'accounts', '0005_emailactivation', '2017-12-10 12:20:34.249477'),
(6, 'billing', '0001_initial', '2017-12-10 12:20:35.311578'),
(7, 'billing', '0002_auto_20170928_2052', '2017-12-10 12:20:36.291839'),
(8, 'addresses', '0001_initial', '2017-12-10 12:20:37.330443'),
(9, 'addresses', '0002_auto_20171107_0055', '2017-12-10 12:20:38.465009'),
(10, 'contenttypes', '0001_initial', '2017-12-10 12:20:39.108853'),
(11, 'admin', '0001_initial', '2017-12-10 12:20:41.054710'),
(12, 'admin', '0002_logentry_remove_auto_add', '2017-12-10 12:20:41.171906'),
(13, 'contenttypes', '0002_remove_content_type_name', '2017-12-10 12:20:42.307575'),
(14, 'analytics', '0001_initial', '2017-12-10 12:20:44.312337'),
(15, 'analytics', '0002_usersession', '2017-12-10 12:20:45.314149'),
(16, 'auth', '0001_initial', '2017-12-10 12:20:49.926371'),
(17, 'auth', '0002_alter_permission_name_max_length', '2017-12-10 12:20:50.178304'),
(18, 'auth', '0003_alter_user_email_max_length', '2017-12-10 12:20:50.250107'),
(19, 'auth', '0004_alter_user_username_opts', '2017-12-10 12:20:50.305097'),
(20, 'auth', '0005_alter_user_last_login_null', '2017-12-10 12:20:50.363649'),
(21, 'auth', '0006_require_contenttypes_0002', '2017-12-10 12:20:50.403329'),
(22, 'auth', '0007_alter_validators_add_error_messages', '2017-12-10 12:20:50.489518'),
(23, 'auth', '0008_alter_user_username_max_length', '2017-12-10 12:20:50.551228'),
(24, 'billing', '0003_billingprofile_customer_id', '2017-12-10 12:20:51.263747'),
(25, 'billing', '0004_card', '2017-12-10 12:20:52.460567'),
(26, 'billing', '0005_card_default', '2017-12-10 12:20:53.125867'),
(27, 'billing', '0006_charge', '2017-12-10 12:20:55.017102'),
(28, 'billing', '0007_auto_20171012_1935', '2017-12-10 12:20:56.247316'),
(29, 'products', '0001_initial', '2017-12-10 12:20:56.514008'),
(30, 'products', '0002_product_price', '2017-12-10 12:20:57.119751'),
(31, 'products', '0003_product_image', '2017-12-10 12:20:57.800600'),
(32, 'products', '0004_auto_20170901_2159', '2017-12-10 12:20:57.849249'),
(33, 'products', '0005_product_featured', '2017-12-10 12:20:58.440752'),
(34, 'products', '0006_auto_20170901_2254', '2017-12-10 12:20:59.925334'),
(35, 'products', '0007_auto_20170901_2254', '2017-12-10 12:21:00.096325'),
(36, 'products', '0008_auto_20170901_2300', '2017-12-10 12:21:00.440333'),
(37, 'products', '0009_product_timestamp', '2017-12-10 12:21:01.262423'),
(38, 'carts', '0001_initial', '2017-12-10 12:24:40.300544'),
(39, 'carts', '0002_cart_subtotal', '2017-12-10 12:24:51.001936'),
(40, 'marketing', '0001_initial', '2017-12-10 12:24:52.329030'),
(41, 'marketing', '0002_marketingpreference_mailchimp_subscribed', '2017-12-10 12:24:52.832598'),
(42, 'marketing', '0003_auto_20171018_0142', '2017-12-10 12:24:53.117230'),
(43, 'products', '0010_product_is_digital', '2017-12-10 12:24:53.828413'),
(44, 'orders', '0001_initial', '2017-12-10 12:26:00.001839'),
(45, 'orders', '0002_auto_20170928_2224', '2017-12-10 12:26:02.195810'),
(46, 'orders', '0003_auto_20170929_0013', '2017-12-10 12:26:05.531444'),
(47, 'orders', '0004_auto_20171025_2216', '2017-12-10 12:26:07.086663'),
(48, 'orders', '0005_auto_20171107_0035', '2017-12-10 12:26:08.339325'),
(49, 'orders', '0006_productpurchase_productpurchasemanager', '2017-12-10 12:26:11.757251'),
(50, 'orders', '0007_auto_20171108_0028', '2017-12-10 12:26:12.955853'),
(51, 'orders', '0008_delete_productpurchasemanager', '2017-12-10 12:26:13.144246'),
(52, 'orders', '0009_auto_20171210_0413', '2017-12-10 12:26:15.035420'),
(53, 'products', '0011_productfile', '2017-12-10 12:26:16.274256'),
(54, 'products', '0012_auto_20171108_2325', '2017-12-10 12:26:16.355327'),
(55, 'products', '0013_auto_20171109_0023', '2017-12-10 12:26:17.381241'),
(56, 'products', '0014_auto_20171116_0011', '2017-12-10 12:26:17.436612'),
(57, 'products', '0015_productfile_name', '2017-12-10 12:26:18.029887'),
(58, 'sessions', '0001_initial', '2017-12-10 12:26:18.576622'),
(59, 'tags', '0001_initial', '2017-12-10 12:26:21.398321');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('fc1jtztviunqrkq1ewas0r9pip1fa0vn', 'YzRiMjc3YjA3YzllNjk1MDQyZmI2MTUxODA4OGNiOTRkZWUwNjFmYjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNzYwYTdjNjNmZTk2ZWI2YjQzOTAzOTUwYzUwN2QxOWM1MWVlOGRjMSIsImNhcnRfaWQiOjMsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2017-12-25 09:56:49.453615');

-- --------------------------------------------------------

--
-- Table structure for table `marketing_marketingpreference`
--

CREATE TABLE `marketing_marketingpreference` (
  `id` int(11) NOT NULL,
  `subscribed` tinyint(1) NOT NULL,
  `mailchimp_msg` longtext,
  `timestamp` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mailchimp_subscribed` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `marketing_marketingpreference`
--

INSERT INTO `marketing_marketingpreference` (`id`, `subscribed`, `mailchimp_msg`, `timestamp`, `updated`, `user_id`, `mailchimp_subscribed`) VALUES
(1, 0, '{u\'status\': 401, u\'instance\': u\'fce58a94-bb6b-4cdc-bc3c-b9fbcf9315a2\', u\'type\': u\'http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/\', u\'detail\': u"Your API key may be invalid, or you\'ve attempted to access the wrong datacenter.", u\'title\': u\'API Key Invalid\'}', '2017-12-10 12:36:34.942734', '2017-12-10 12:36:34.942879', 3, 0),
(2, 0, '{u\'status\': 401, u\'instance\': u\'b0de02b1-befe-440b-93f8-4ad351a621d2\', u\'type\': u\'http://developer.mailchimp.com/documentation/mailchimp/guides/error-glossary/\', u\'detail\': u"Your API key may be invalid, or you\'ve attempted to access the wrong datacenter.", u\'title\': u\'API Key Invalid\'}', '2017-12-11 10:20:09.494923', '2017-12-11 10:20:09.495060', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders_order`
--

CREATE TABLE `orders_order` (
  `id` int(11) NOT NULL,
  `order_id` varchar(120) NOT NULL,
  `status` varchar(120) NOT NULL,
  `shipping_total` decimal(10,1) NOT NULL,
  `total` decimal(10,1) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `billing_profile_id` int(11) DEFAULT NULL,
  `billing_address_id` int(11) DEFAULT NULL,
  `shipping_address_id` int(11) DEFAULT NULL,
  `timestamp` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `billing_address_final` longtext,
  `shipping_address_final` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders_productpurchase`
--

CREATE TABLE `orders_productpurchase` (
  `id` int(11) NOT NULL,
  `refunded` tinyint(1) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `billing_profile_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products_product`
--

CREATE TABLE `products_product` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `description` longtext NOT NULL,
  `price` decimal(20,2) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `is_digital` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_product`
--

INSERT INTO `products_product` (`id`, `title`, `description`, `price`, `image`, `featured`, `active`, `slug`, `timestamp`, `is_digital`) VALUES
(1, 'sadasd', 'asdada', '40.00', '', 0, 1, 'sadasd', '2017-12-10 14:52:40.377227', 0),
(2, 'Manuskrip Jaman Kuno', 'Adalah Sebuah Manuskrip yang berasal dari jaman 1892 penerbit oleh kyai a', '0.00', NULL, 0, 0, '', '2017-12-11 00:00:00.000000', 1),
(5, 'test', 'Adalah Sebuah Manuskrip yang berasal dari jaman 1892 penerbit oleh kyai a', '0.00', '', 0, 1, '12', '2017-12-11 10:26:48.246189', 0),
(6, 'Jamano 2', 'Adalah Sebuah', '0.00', '', 0, 1, '0000000', '2017-12-11 10:27:47.105929', 0);

-- --------------------------------------------------------

--
-- Table structure for table `products_productfile`
--

CREATE TABLE `products_productfile` (
  `id` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  `product_id` int(11) NOT NULL,
  `free` tinyint(1) NOT NULL,
  `user_required` tinyint(1) NOT NULL,
  `name` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags_tag`
--

CREATE TABLE `tags_tag` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `timestamp` datetime(6) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags_tag_products`
--

CREATE TABLE `tags_tag_products` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts_emailactivation`
--
ALTER TABLE `accounts_emailactivation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accounts_emailactivation_user_id_c8c6527b_fk_accounts_user_id` (`user_id`);

--
-- Indexes for table `accounts_guestemail`
--
ALTER TABLE `accounts_guestemail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `accounts_user`
--
ALTER TABLE `accounts_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `addresses_address`
--
ALTER TABLE `addresses_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_address_billing_profile_id_115cdf27_fk_billing_b` (`billing_profile_id`);

--
-- Indexes for table `analytics_objectviewed`
--
ALTER TABLE `analytics_objectviewed`
  ADD PRIMARY KEY (`id`),
  ADD KEY `analytics_objectview_content_type_id_35d996a4_fk_django_co` (`content_type_id`),
  ADD KEY `analytics_objectviewed_user_id_b1e9cf2a_fk_accounts_user_id` (`user_id`);

--
-- Indexes for table `analytics_usersession`
--
ALTER TABLE `analytics_usersession`
  ADD PRIMARY KEY (`id`),
  ADD KEY `analytics_usersession_user_id_548abc25_fk_accounts_user_id` (`user_id`);

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `billing_billingprofile`
--
ALTER TABLE `billing_billingprofile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `billing_card`
--
ALTER TABLE `billing_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billing_card_billing_profile_id_a1cc4bbe_fk_billing_b` (`billing_profile_id`);

--
-- Indexes for table `billing_charge`
--
ALTER TABLE `billing_charge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billing_charge_billing_profile_id_8bdb625d_fk_billing_b` (`billing_profile_id`);

--
-- Indexes for table `carts_cart`
--
ALTER TABLE `carts_cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carts_cart_user_id_bd0756c7_fk_accounts_user_id` (`user_id`);

--
-- Indexes for table `carts_cart_products`
--
ALTER TABLE `carts_cart_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `carts_cart_products_cart_id_product_id_75f0c8ea_uniq` (`cart_id`,`product_id`),
  ADD KEY `carts_cart_products_product_id_17f38e1e_fk_products_product_id` (`product_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_accounts_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `marketing_marketingpreference`
--
ALTER TABLE `marketing_marketingpreference`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `orders_order`
--
ALTER TABLE `orders_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_order_cart_id_7e0252e3_fk_carts_cart_id` (`cart_id`),
  ADD KEY `orders_order_billing_profile_id_0e11b610_fk_billing_b` (`billing_profile_id`),
  ADD KEY `orders_order_billing_address_id_deb02e83_fk_addresses_address_id` (`billing_address_id`),
  ADD KEY `orders_order_shipping_address_id_c4f8227a_fk_addresses` (`shipping_address_id`);

--
-- Indexes for table `orders_productpurchase`
--
ALTER TABLE `orders_productpurchase`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_productpurcha_billing_profile_id_4c08eaae_fk_billing_b` (`billing_profile_id`),
  ADD KEY `orders_productpurcha_product_id_d0f20172_fk_products_` (`product_id`);

--
-- Indexes for table `products_product`
--
ALTER TABLE `products_product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_product_slug_70d3148d_uniq` (`slug`);

--
-- Indexes for table `products_productfile`
--
ALTER TABLE `products_productfile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_productfile_product_id_34f142ef_fk_products_product_id` (`product_id`);

--
-- Indexes for table `tags_tag`
--
ALTER TABLE `tags_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tags_tag_slug_78c2b8d8` (`slug`);

--
-- Indexes for table `tags_tag_products`
--
ALTER TABLE `tags_tag_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_tag_products_tag_id_product_id_ed6e4461_uniq` (`tag_id`,`product_id`),
  ADD KEY `tags_tag_products_product_id_f64ffb65_fk_products_product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts_emailactivation`
--
ALTER TABLE `accounts_emailactivation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `accounts_guestemail`
--
ALTER TABLE `accounts_guestemail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `accounts_user`
--
ALTER TABLE `accounts_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `addresses_address`
--
ALTER TABLE `addresses_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `analytics_objectviewed`
--
ALTER TABLE `analytics_objectviewed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `analytics_usersession`
--
ALTER TABLE `analytics_usersession`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `billing_billingprofile`
--
ALTER TABLE `billing_billingprofile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `billing_card`
--
ALTER TABLE `billing_card`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `billing_charge`
--
ALTER TABLE `billing_charge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `carts_cart`
--
ALTER TABLE `carts_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `carts_cart_products`
--
ALTER TABLE `carts_cart_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT for table `marketing_marketingpreference`
--
ALTER TABLE `marketing_marketingpreference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders_order`
--
ALTER TABLE `orders_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders_productpurchase`
--
ALTER TABLE `orders_productpurchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products_product`
--
ALTER TABLE `products_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `products_productfile`
--
ALTER TABLE `products_productfile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags_tag`
--
ALTER TABLE `tags_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags_tag_products`
--
ALTER TABLE `tags_tag_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts_emailactivation`
--
ALTER TABLE `accounts_emailactivation`
  ADD CONSTRAINT `accounts_emailactivation_user_id_c8c6527b_fk_accounts_user_id` FOREIGN KEY (`user_id`) REFERENCES `accounts_user` (`id`);

--
-- Constraints for table `addresses_address`
--
ALTER TABLE `addresses_address`
  ADD CONSTRAINT `addresses_address_billing_profile_id_115cdf27_fk_billing_b` FOREIGN KEY (`billing_profile_id`) REFERENCES `billing_billingprofile` (`id`);

--
-- Constraints for table `analytics_objectviewed`
--
ALTER TABLE `analytics_objectviewed`
  ADD CONSTRAINT `analytics_objectview_content_type_id_35d996a4_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `analytics_objectviewed_user_id_b1e9cf2a_fk_accounts_user_id` FOREIGN KEY (`user_id`) REFERENCES `accounts_user` (`id`);

--
-- Constraints for table `analytics_usersession`
--
ALTER TABLE `analytics_usersession`
  ADD CONSTRAINT `analytics_usersession_user_id_548abc25_fk_accounts_user_id` FOREIGN KEY (`user_id`) REFERENCES `accounts_user` (`id`);

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `billing_billingprofile`
--
ALTER TABLE `billing_billingprofile`
  ADD CONSTRAINT `billing_billingprofile_user_id_1a5112fa_fk_accounts_user_id` FOREIGN KEY (`user_id`) REFERENCES `accounts_user` (`id`);

--
-- Constraints for table `billing_card`
--
ALTER TABLE `billing_card`
  ADD CONSTRAINT `billing_card_billing_profile_id_a1cc4bbe_fk_billing_b` FOREIGN KEY (`billing_profile_id`) REFERENCES `billing_billingprofile` (`id`);

--
-- Constraints for table `billing_charge`
--
ALTER TABLE `billing_charge`
  ADD CONSTRAINT `billing_charge_billing_profile_id_8bdb625d_fk_billing_b` FOREIGN KEY (`billing_profile_id`) REFERENCES `billing_billingprofile` (`id`);

--
-- Constraints for table `carts_cart`
--
ALTER TABLE `carts_cart`
  ADD CONSTRAINT `carts_cart_user_id_bd0756c7_fk_accounts_user_id` FOREIGN KEY (`user_id`) REFERENCES `accounts_user` (`id`);

--
-- Constraints for table `carts_cart_products`
--
ALTER TABLE `carts_cart_products`
  ADD CONSTRAINT `carts_cart_products_cart_id_3124fdb4_fk_carts_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `carts_cart` (`id`),
  ADD CONSTRAINT `carts_cart_products_product_id_17f38e1e_fk_products_product_id` FOREIGN KEY (`product_id`) REFERENCES `products_product` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_accounts_user_id` FOREIGN KEY (`user_id`) REFERENCES `accounts_user` (`id`);

--
-- Constraints for table `marketing_marketingpreference`
--
ALTER TABLE `marketing_marketingpreference`
  ADD CONSTRAINT `marketing_marketingp_user_id_b28021e1_fk_accounts_` FOREIGN KEY (`user_id`) REFERENCES `accounts_user` (`id`);

--
-- Constraints for table `orders_order`
--
ALTER TABLE `orders_order`
  ADD CONSTRAINT `orders_order_billing_address_id_deb02e83_fk_addresses_address_id` FOREIGN KEY (`billing_address_id`) REFERENCES `addresses_address` (`id`),
  ADD CONSTRAINT `orders_order_billing_profile_id_0e11b610_fk_billing_b` FOREIGN KEY (`billing_profile_id`) REFERENCES `billing_billingprofile` (`id`),
  ADD CONSTRAINT `orders_order_cart_id_7e0252e3_fk_carts_cart_id` FOREIGN KEY (`cart_id`) REFERENCES `carts_cart` (`id`),
  ADD CONSTRAINT `orders_order_shipping_address_id_c4f8227a_fk_addresses` FOREIGN KEY (`shipping_address_id`) REFERENCES `addresses_address` (`id`);

--
-- Constraints for table `orders_productpurchase`
--
ALTER TABLE `orders_productpurchase`
  ADD CONSTRAINT `orders_productpurcha_billing_profile_id_4c08eaae_fk_billing_b` FOREIGN KEY (`billing_profile_id`) REFERENCES `billing_billingprofile` (`id`),
  ADD CONSTRAINT `orders_productpurcha_product_id_d0f20172_fk_products_` FOREIGN KEY (`product_id`) REFERENCES `products_product` (`id`);

--
-- Constraints for table `products_productfile`
--
ALTER TABLE `products_productfile`
  ADD CONSTRAINT `products_productfile_product_id_34f142ef_fk_products_product_id` FOREIGN KEY (`product_id`) REFERENCES `products_product` (`id`);

--
-- Constraints for table `tags_tag_products`
--
ALTER TABLE `tags_tag_products`
  ADD CONSTRAINT `tags_tag_products_product_id_f64ffb65_fk_products_product_id` FOREIGN KEY (`product_id`) REFERENCES `products_product` (`id`),
  ADD CONSTRAINT `tags_tag_products_tag_id_7ed0fcd2_fk_tags_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `tags_tag` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
